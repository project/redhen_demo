<?php
/**
 * @file
 * redhen_features.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function redhen_features_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'body'
  $field_bases['body'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'node',
    ),
    'field_name' => 'body',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_address'
  $field_bases['field_address'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_address',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'addressfield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'addressfield',
  );

  // Exported field_base: 'field_animals'
  $field_bases['field_animals'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_animals',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Cats' => 'Cats',
        'Dogs' => 'Dogs',
        'Fish' => 'Fish',
        'Horses' => 'Horses',
        'Snakes' => 'Snakes',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_availability'
  $field_bases['field_availability'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_availability',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_donation'
  $field_bases['field_donation'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_donation',
    'foreign keys' => array(
      'redhen_donation_type' => array(
        'columns' => array(
          'redhen_donation_type' => 'name',
        ),
        'table' => 'redhen_donation_type',
      ),
    ),
    'indexes' => array(
      'redhen_donation_type' => array(
        0 => 'redhen_donation_type',
      ),
    ),
    'locked' => 0,
    'module' => 'redhen_donation',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'redhen_donation',
  );

  // Exported field_base: 'field_registration'
  $field_bases['field_registration'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_registration',
    'foreign keys' => array(
      'registration_type' => array(
        'columns' => array(
          'registration_type' => 'name',
        ),
        'table' => 'registration_type',
      ),
    ),
    'indexes' => array(
      'registration_type' => array(
        0 => 'registration_type',
      ),
    ),
    'locked' => 0,
    'module' => 'registration',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'registration',
  );

  // Exported field_base: 'redhen_contact_email'
  $field_bases['redhen_contact_email'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'redhen_contact_email',
    'foreign keys' => array(),
    'indexes' => array(
      'redhen_email_address' => array(
        0 => 'value',
      ),
    ),
    'locked' => 1,
    'module' => 'redhen_fields',
    'settings' => array(
      'labels' => array(
        0 => 'home',
        1 => 'work',
      ),
    ),
    'translatable' => 0,
    'type' => 'redhen_email',
  );

  return $field_bases;
}
