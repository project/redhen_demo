# RedHen CRM - Demonstration Install Profile

Brought to you by those friendly geeks at [ThinkShout, Inc.](http://thinkshout.com)

## Notes:

* This install profile is a work in progress.
* This install profile is meant for demonstration and testing purposes only.
(It is far from a starting point for production RedHen builds.)

## Installation

* Run the build script from the root profile directory.
* Usage: ./scripts/build.sh [-y] <DESTINATION_PATH> <DB_USER> <DB_PASS> <DB_NAME>
* The last 3 parameters are used to run the installation profile. They can be
omitted to just build the distribution.
* Alternatively, you can run Drush make on your own per any distribution.

## RedHen Donation

See https://www.drupal.org/node/2275247
