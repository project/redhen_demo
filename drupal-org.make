api = 2
core = 7.x

; defaults
defaults[projects][subdir] = "contrib"

; Standard Contrib Modules
projects[] = addressfield
projects[] = admin_menu
projects[] = ctools
projects[] = diff
projects[] = devel
projects[] = features
projects[] = r4032login
projects[] = strongarm
projects[] = token
projects[] = views
projects[] = entityreference

projects[entity][version] = 1.5
projects[relation][version] = 1.0-rc5
projects[message][version] = 1.9
projects[libraries][version] = 2.2
projects[registration][version] = 1.3

; RedHen Module Suite
projects[redhen][version] = 1.7
projects[redhen_membership][version] = 1.0

; RedHen Donation
projects[redhen_donation][version] = 1.0-beta2

; Donation dependencies
; date
projects[date][version] = "2.7"
projects[date][subdir] = "contrib"

; libraries
projects[libraries][version] = "2.1"
projects[libraries][subdir] = "contrib"

; rules
projects[rules][version] = "2.7"
projects[rules][subdir] = "contrib"

; select or other
projects[select_or_other][version] = "2.20"
projects[select_or_other][subdir] = "contrib"

; commerce
projects[commerce][version] = "1.9"
projects[commerce][subdir] = "contrib"

;interval
projects[interval][version] = "1.0"
projects[interval][subdir] = "contrib"

; commerce_recurring
projects[commerce_recurring][subdir] = "contrib"
projects[commerce_recurring][download][type] = "git"
projects[commerce_recurring][download][branch] = "7.x-2.x"
projects[commerce_recurring][download][revision] = "18766b3"
; Allow for custom order types - https://drupal.org/node/2273443
projects[commerce_recurring][patch][] = "https://drupal.org/files/issues/commerce_recurring-custom_order_types-2273443-1.patch"
; Allow Initial and Recurring price to be altered when Recurring orders are created - https://drupal.org/node/2263371
projects[commerce_recurring][patch][] = "https://drupal.org/files/issues/commerce_recurring-Alter_recurring_price-2263371-1.patch"

;commerce_cardonfile
projects[commerce_cardonfile][version] = "2.0-beta5"
projects[commerce_cardonfile][subdir] = "contrib"
; Returned method should only expect FALSE - https://drupal.org/node/2275263
projects[commerce_cardonfile][patch][] = "https://drupal.org/files/issues/commerce_cardonfile-Returned_method_should_only_expect_FALSE-2275263-1.patch"

; Themes
projects[zen][version] = 5.5
projects[zen][subdir] = ""

projects[poultry][type] = "theme"
projects[poultry][download][type] = "git"
projects[poultry][download][url] = "http://git.drupal.org/project/poultry.git"
projects[poultry][download][branch] = "7.x-1.x"
projects[poultry][subdir] = ""
